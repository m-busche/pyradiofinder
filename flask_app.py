from flask import Flask, render_template, request
from radio import getRadios
import subprocess
app = Flask(__name__)

try:
    gitversion = subprocess.check_output(['git', 'rev-parse', '--short', 'HEAD'], cwd="/home/elpatronKI/pyradiofinder/").decode('ascii').strip()
except:
    gitversion = "n/a"
    
@app.route('/')
def index():
    return render_template('index.html', gitversion=gitversion, numresults = -1)

@app.route('/', methods=['POST'])
def my_form_post():
    text = request.form['Suchbegriff']
    exactsearch = len(request.form.getlist('exact'))
    if exactsearch == 0:
        e = False
    else:
        e = True
    result = getRadios(text, e, False)
    return render_template("index.html", result = result, numresults = len(result), query=text, gitversion=gitversion)

@app.route('/about')
def about():
    return render_template('about.html', gitversion=gitversion)

def get_git_revision_short_hash() -> str:
    return subprocess.check_output(['git', 'rev-parse', '--short', 'HEAD'], cwd="/home/elpatronKI/pyradiofinder/").decode('ascii').strip()

if __name__ == '__main__':
    app.run(host='127.0.0.1', port=8089, debug=False)
    