import sys
from pyradios import RadioBrowser
rb = RadioBrowser()

def getRadios(query, exact, printresults):
    results = rb.search(name=query, name_exact=exact)
    i = 0
    
    extendedresults = []
    for r in results:
        r['shortname'] = r['name'][:64]
        # a = ["nan" if x == '' else x for x in a]
        r['infotooltip'] = 'Tags: ' +  (r['tags'] if r['tags'] != '' else 'n/a') + '\n' + \
                           'Language: ' + (r['language'] if r['language'] != '' else 'n/a') + '\n' +\
                           'Last change: ' + r['lastchangetime'] + '\n' +\
                           'Last check: ' + r['lastchecktime']
        r['countrycode'] = (r['countrycode'] if r['countrycode'] != '' else 'n/a')
        if r['lastcheckok'] == 1:
            extendedresults.append(r)
        
        if printresults:
            i += 1
            print('[' + str(i) + ']')
            print('Name: ' + r['name'] + 
                '\nURL: ' + r['url'] + 
                '\nTags: ' + r['tags'] + 
                '\nCountry: ' + r['country'] + 
                '\nHomepage: ' + r['homepage'] + 
                '\n'
                )
        
    return extendedresults

    
if __name__ == "__main__":
    if len(sys.argv) == 1:
        value = input("Radio station search:\n")
    else:
        value = str(sys.argv[1])
    getRadios(value, False, True)