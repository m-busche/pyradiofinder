FROM python:3.11
WORKDIR /app
COPY ./flask_app.py /app
COPY ./radio.py /app
COPY ./wsgi.py /app
COPY ./requirements.txt /app
COPY ./static/ /app/static/
COPY ./templates/* /app/templates/
RUN pip install --no-cache-dir --upgrade -r requirements.txt
CMD ["gunicorn", "--bind", "0.0.0.0:5000", "wsgi:app"]
